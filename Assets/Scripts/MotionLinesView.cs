﻿using UnityEngine;
using UnityEngine.UI;

namespace Rayark.Hi
{
    public class MotionLinesView : MonoBehaviour
    {
        [SerializeField]
        private Image _motionLinesImg;
        [SerializeField]
        private float _minDisplaySpeed;
        [SerializeField]
        private float _maxDisplaySpeed;
        [SerializeField]
        private float _minDisplayAlpha;

        public void SetImgAlphaBySpeed(float speedValue)
        {
            float finalAlpha = 0;
            if (speedValue >= _minDisplaySpeed)
            {
                finalAlpha = _minDisplayAlpha + Mathf.InverseLerp(_minDisplaySpeed, _maxDisplaySpeed, speedValue) * (1-_minDisplayAlpha);
            }
            _motionLinesImg.color = new Color(
                _motionLinesImg.color.r,
                _motionLinesImg.color.g,
                _motionLinesImg.color.b,
                finalAlpha);
        }
    }
}
