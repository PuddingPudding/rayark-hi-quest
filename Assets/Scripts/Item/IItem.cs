﻿using UnityEngine;

namespace Rayark.Hi.Engine
{
    public interface IItem
    {
        Vector2 Size { get; }
        /// <summary>
        /// 生成的編號
        /// </summary>
        float GeneratedProbability { get; }

        void GetEffect(IEffect effect);
    }
    
}