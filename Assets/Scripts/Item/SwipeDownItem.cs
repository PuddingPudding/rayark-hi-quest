﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Rayark.Hi.Engine
{
    public class SwipeDownItem : IItem
    {
        private const int SWIPE_CHANGE_AMOUNT = -1;

        private Vector2 _size;

        public SwipeDownItem(Vector2 size)
        {
            _size = size;
        }

        Vector2 IItem.Size
        {
            get { return _size; }
        }

        float IItem.GeneratedProbability
        {
            get { return 0.5f; }
        }

        void IItem.GetEffect(IEffect effect)
        {
            effect.ChangeRemainSwipeCount(SWIPE_CHANGE_AMOUNT);
        }
    }
}