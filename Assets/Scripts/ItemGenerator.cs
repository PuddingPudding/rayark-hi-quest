﻿using System.Collections.Generic;
using UnityEngine;

namespace Rayark.Hi
{
    public class ItemGenerator : MonoBehaviour //每個道具個別的生成器
    {
        /// <summary>
        /// 每個道具的基本資料，位置&是否被用過
        /// </summary>
        public class ItemData
        {
            public Vector3 Position;
            public bool IsUsed;
        }

        [SerializeField]
        private Transform _itemRootTransform;

        [SerializeField]
        private GameObject _itemPrefab;

        private List<GameObject> _itemInstances = new List<GameObject>();

        private bool _isDisplay = true;

        /// <summary>
        /// 設定是否顯示已用過的道具
        /// </summary>
        /// <param name="isDisplay"></param>
        public void SetDisplayUsedItem(bool isDisplay)
        {
            _isDisplay = isDisplay;
        }

        public void UpdateItems(ItemData[] items)
        {
            if(items.Length > _itemInstances.Count)
            {
                for (int i = _itemInstances.Count; i < items.Length; ++i) //檢查帶入的道具數量是否多過生成器管理的數量
                {
                    _itemInstances.Add(Instantiate(_itemPrefab, _itemRootTransform)); //有的話就新增
                }
            }

            var itemInstances = _itemInstances.ToArray();
            for(int i = 0; i < items.Length; ++i)
            {
                itemInstances[i].SetActive(!items[i].IsUsed || _isDisplay);
                itemInstances[i].transform.localPosition = items[i].Position;
                
            }
        }

        public void ReleaseObjects()
        {
            foreach(var instance in _itemInstances)
            {
                Destroy(instance);
            }
            _itemInstances.Clear();
        }
    }
}