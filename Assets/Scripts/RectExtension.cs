﻿using UnityEngine;

namespace Rayark.Hi
{
    public static class RectExtension
    {
        /// <summary>
        /// 原本的Rect是帶入角落座標與邊長，這邊則改為中心點和大小資訊就好
        /// </summary>
        /// <param name="centerPosition"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public static Rect ToRect(Vector2 centerPosition, Vector2 size)
        {
            return new Rect(centerPosition.x - size.x / 2, centerPosition.y - size.y / 2,
                size.x, size.y);
        }
    }
}